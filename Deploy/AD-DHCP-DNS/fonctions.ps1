#Fonction permettant d'installer un rôle passé en paramètre
function Install-role{
    param (
        # Nom du rôle à installer
        [Parameter(Mandatory=$True)]
        [string] $role
    )
    try {
        Install-WindowsFeature $role -ErrorAction Stop
        Write-Host -Fore 'Green' "Le rôle " $role " a été correctement installé"
        
    }
    catch {
        Write-Host -Fore 'Red' "Erreur 01, Impossible d'installer le rôle " $role
    }
}

function Configure-DHCP{
    param (
        
        )
        
        if ((get-windowsfeature | where name -eq DHCP).Installstate -eq "Installed") {
            Write-Host " Rôle Installé, configuration en cours"
            Throw "Erreur 03, Le rôle DHCP n'est pas installé"
        } 
    else {
    }
}

