#---------------------------------------------------------------------------------
# Copy an existing and user and create an another one
# Used in hybrid environnement
# Auto add mailbox and licence and force sync to AAD
#---------------------------------------------------------------------------------
#Tristan LANOY
#2019
#--------------------------------------------------------------------------------- 

#Parameters
# Exchange server 
$exchangeS = "EXCH2016.company.tld"
# AAD Connect server
$aadconnectS = "AAD-CONNECT.company.tld"
#Domain
$domaineD = "company.tld"
#Microsoft 365 teant domain (value between @ and onmicrosoft.com)
$office365D = "company"
#Licence Microsoft 365 (Get-MsolAccountSku)
$licence = "company:STANDARDPACK"


# Import ActiveDirectory Module
If (!(Get-module ActiveDirectory )) {
    Import-Module ActiveDirectory
    Clear-Host
    }


#Asking for vars
$modeleU = Read-Host "User to copy (user name) : "
$firstnameU = Read-Host "First name of the user to create (Ex: Harry) : "
$lastnameU = Read-Host "Last name of the user to create (Ex: COVERT) : "
$usernameU = Read-Host "User name of the user to create (Ex: h.covert ) : "
$aliasU = Read-Host -Prompt "Alias of the user to create (Ex: harry.covert) : "
$passwordU = Read-Host -Prompt "Password of the user to create : "
$phoneU = Read-Host -Prompt "Phone number of the user to create, leave empty if unknown : "

#Password converter
$SecurePassword=ConvertTo-SecureString $passwordU –asplaintext –force

#Check if user name already exists
$unique = $False
if (@(Get-ADUser -Filter { SamAccountName -eq $usernameU }).Count -eq 0) {
    $unique = $True
}

while ($unique -eq $False) {
    Write-Host "This username already exists, please choose another one"
    $usernameU = Read-Host "User name of the user to create (Ex: h.covert ) : "
    if (@(Get-ADUser -Filter { SamAccountName -eq $usernameU }).Count -eq 0) {
        $unique = $True
    }
}





#Retrieval of data from already existing user
$modele = Get-ADUser -Identity $modeleU
$logon  = Get-ADUser -Identity $modeleU -properties scriptpath | Select-Object -ExpandProperty scriptpath
$city  = Get-ADUser -Identity $modeleU -properties City | Select-Object -ExpandProperty City
$description  = Get-ADUser -Identity $modeleU -properties City | Select-Object -ExpandProperty City

#Connecing to exchange on prem
Write-Host "   "
Write-Host "`n `n `n `n °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°° `n Domain admin credentials in the form:  DOMAIN\AdminUsername `n °°°°°°°°°°°°°°°°°°°°°°°°°°°°°° `n `n `n"
$Credentials = Get-Credential
$ExSession = New-PSSession –ConfigurationName Microsoft.Exchange –ConnectionUri "http://$exchangeS/PowerShell/?SerializationLevel=Full" -Credential $Credentials
Import-PSSession $ExSession -AllowClobber

#Retrieval of OU
$ou = Get-remotemailbox $modeleU | select-object -expand OnPremisesOrganizationalUnit

#User creation
New-RemoteMailbox -UserPrincipalName "$aliasU@$domaineD" -RemoteRoutingAddress "$usernameU@$office365D.mail.onmicrosoft.com" -SamAccountName "$usernameU" -PrimarySmtpAddress "$aliasU@$domaineD" -Alias "$aliasU" -Name $usernameU -FirstName "$firstnameU" -LastName "$lastnameU" -DisplayName "$firstnameU $lastnameU" -OnPremisesOrganizationalUnit $ou -Password $SecurePassword -ResetPasswordOnNextLogon $False
Set-RemoteMailbox $usernameU -RemoteRoutingAddress "$usernameU@$office365D.mail.onmicrosoft.com"
Set-ADUser -Identity $usernameU -Add @{Proxyaddresses="smtp:$usernameU@$office365D.mail.onmicrosoft.com"}

#Copy the groups of the user to be copied
Get-ADUser -Identity $modeleU -Properties memberof | Select-Object -ExpandProperty memberof |  Add-ADGroupMember -Members $usernameU

#Copy login script
Set-ADUser -Identity $usernameU -ScriptPath $logon

#Add phone number
Set-ADUser $usernameU -Replace @{telephoneNumber="$phoneU"}

#AD and AAD force sync
$s = New-PSSession -ComputerName "$aadconnectS"
Invoke-Command -Session $s -ScriptBlock {Import-Module "C:\Program Files\Microsoft Azure AD Sync\Bin\ADSync\ADSync.psd1"}
Invoke-Command -Session $s -ScriptBlock {Start-ADSyncSyncCycle -PolicyType Delta}
Remove-PSSession $s

#Wait for sync
Start-Sleep -s 30

#Connect to Office 365 et assign licence
Import-Module MSOnline
Connect-MsolService
Set-MsolUser -UserPrincipalName "$aliasU@$domaineD" -UsageLocation FR
Set-MsolUserLicense -UserPrincipalName "$aliasU@$domaineD" -AddLicenses "$licence"



#End
Write-host "Script completed, close in 15 sec"
Start-Sleep -s 15
