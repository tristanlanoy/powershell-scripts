#---------------------------------------------------------------------------------
# Disable Self Service Purchase in M365
#---------------------------------------------------------------------------------
#Unknow source
#2020
#--------------------------------------------------------------------------------- 

#Install-Module MSCommerce
Import-Module -Name MSCommerce 
Connect-MSCommerce #sign-in with your global or billing administrator account when prompted 
Get-MSCommerceProductPolicies -PolicyId AllowSelfServicePurchase | Where { $_.PolicyValue -eq “Enabled”} | forEach { Update-MSCommerceProductPolicy -PolicyId AllowSelfServicePurchase -ProductId $_.ProductID -Enabled $false }