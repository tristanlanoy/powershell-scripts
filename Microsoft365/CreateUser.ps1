#---------------------------------------------------------------------------------
# Create users based on csv file
# Csv file headers :
# mail,display,password,fname,sname;-,license
# Get licence id : Get-MsolAccountSku
#---------------------------------------------------------------------------------
#Tristan LANOY
#2019
#--------------------------------------------------------------------------------- 

# Parameters
$csv = "PathToCsvFile.csv"
$results = "PathToCsvFile.csv"


Import-CSV -Delimiter ";" $csv| 
foreach {New-MsolUser 
-UserPrincipalName $_.mail 
-DisplayName $_.display 
-Password $_.password
-FirstName $_.fname 
-LastName $_.sname
-UsageLocation FR 
-LicenseAssignment $_.license
-PasswordNeverExpire:$true
-ForceChangePassword:$false } | Export-Csv -Path $results
