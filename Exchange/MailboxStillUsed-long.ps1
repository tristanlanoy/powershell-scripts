 
 
#Parameters
$csv = "CsvPath"
$StartDate = 
$EndDate = 
$ResultMailbox = "SendResults@ToThis.mailbox"
 
Import-CSV  $csv | 
foreach {
$trace = "Report for : " + $_.alias;
Start-HistoricalSearch -ReportTitle $trace -StartDate (Get-Date).AddDays(-1) -EndDate (Get-Date) -ReportType MessageTraceDetail -RecipientAddress $_.alias -NotifyAddress $ResultMailbox –DeliveryStatus Delivered
}
 
