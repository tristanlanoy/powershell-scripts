#---------------------------------------------------------------------------------
# Export mailboxes to pst based on csv file
#---------------------------------------------------------------------------------
#Tristan LANOY
#2020
#--------------------------------------------------------------------------------- 

#----------Vars----------

#CSV file path
$PathUsersList = "./UsersList.csv"

#Log file path
$PathLogFile = "./export.log"

#PST folder UNC path
$PathPstFolder = "\\Serveur\Share\PSTfolder\"

#-------------------------


# CSV file import
$Mailboxes = Import-Csv $PathUsersList -Delimiter ";"



foreach ($Mailbox in $Mailboxes)
{
    $PathPST = $PathPstFolder+$Mailbox.Alias+".pst"
    $time = Get-Date
    try 
    {
        New-MailboxExportRequest -Mailbox $Mailbox.Alias -baditemlimit 50 -acceptlargedataloss -FilePath $PathPST
        Add-Content $PathLogFile "$time --- Export Request added for $Mailbox"
    }

    catch
    {
        Add-Content $PathLogFile "$time --- Unable to add export request for $Mailbox"
    }
}
