#---------------------------------------------------------------------------------
#Set UPN to primary Smtp
# Will set primary UPN of user to primary smtp address
# Usefull for AzureAd
#---------------------------------------------------------------------------------
#Tristan LANOY
#2019
#--------------------------------------------------------------------------------- 

Import-Module Active-Directory

$OU = 
foreach ($user in (Get-ADUser -Filter * -SearchBase $OU -properties * |where {$_.emailaddress} )) {
$userdetails = $user | get-aduser -properties *
$user | set-aduser -UserPrincipalName $userdetails.EmailAddress
}
