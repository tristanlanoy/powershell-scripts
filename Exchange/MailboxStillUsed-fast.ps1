#---------------------------------------------------------------------------------
# Check if mailbox still receive mails and get last logon time
# Get mailbox from CSV file with header $_.alias
#---------------------------------------------------------------------------------
#Tristan LANOY
#2019
#--------------------------------------------------------------------------------- 

#Parameters
$csv = "CsvPath"
$StartDate = 
$EndDate = 

Import-CSV  $csv | 
foreach {
Get-MailboxStatistics -Identity $_.alias | Select-Object DisplayName,LastLogonTime ;
Get-MessageTrace -RecipientAddress $_.alias -StartDate $StartDate -EndDate $EndDate
}

 
