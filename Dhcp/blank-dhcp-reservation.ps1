#---------------------------------------------------------------------------------
# Mass creation of DHCP reservation
# Will create DHCP reservation with fake MAC Address
# Can easily be edited to import from a CSV file
#---------------------------------------------------------------------------------
#Tristan LANOY
#2021
#--------------------------------------------------------------------------------- 

#Parameters
$scope="192.168.1."
$server=""

#Var init
$i=0
$ipAdd=0
$ip=""
$mac=""
$name=""

for($i=10;$i -lt 254;$i++)
{
    $ipAdd++
    $ip=$scope+$ipAdd.toString()
    $name="Free "+$ipAdd.ToString()
    $mac ="00060000000"+$ipAdd.ToString()
    Add-dhcpServerv4Reservation -ScopeId $scope"0" -ComputerName $server -IpAddress $ip -Name $name -ClientId $mac
}