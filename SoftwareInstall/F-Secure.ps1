#---------------------------------------------------------------------------------
# Install F-Secure PSB-4 with licence
# I had greater success with this script rather than using GPO integrated software deploys
#---------------------------------------------------------------------------------
#Tristan LANOY
#2020
#--------------------------------------------------------------------------------- 

#Parameters
$path_deploy = "\\path\\to\\DeployShare"
$path_dest = "C:\Temp_Deploy"
$soft = "F-SecurePSB4.msi"
$path_to_test = "HKLM:\SOFTWARE\F-Secure"
$path_log = "\\path\to\Logs$\deploy_fsecure.log"



if (-Not ($path_to_test | Test-Path)) 
{
    $time = Get-Date
    Add-Content $path_log "$time --- $env:computername Computer marked for installation "

    #Create temp folder
    try {
        mkdir $path_dest
        Add-Content $path_log "$time --- $env:computername Folder created "
    }
    catch {
         Add-Content $path_log "$time --- $env:computername Unable to create folder "
    }

    #Copy install file
    try {
        Copy-Item $path_deploy -Destination $path_dest
        Add-Content $path_log "$time --- $env:computername File copied"
    }
    catch {
         Add-Content $path_log "$time --- $env:computername Unable to copy file "
    }

    #Install F-Secire
    try {
        start $path_deploy
        Add-Content $path_log "$time --- $env:computername Software properly installed "
    }
    catch {
         Add-Content $path_log "$time --- $env:computername Unable to install software "
    }

    Remove-Item $path_deploy
    
}